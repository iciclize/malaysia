var request = require('request');
var fs = require('fs');
var mkdirp = require('mkdirp');
var async = require('async');
var gm = require('gm');

var setting = require('./setting.json');
request = request.defaults({
    headers: {
        Cookie: setting.cookie,
        'User-Agent': setting.userAgent
    }
});

// 57807259から57812150まで。欠番あり。
const TARGET = "https://www.photospot.jp/v2/thumbnail/rupetile/55561/{{number}}/{{x}}/{{y}}";

var start = 57809177;
var end = 57812150;

main();

function main() {
    var targets = [];
    // for (var i = 57807259; i <= 57812150; i++)
    for (var i = start; i <= end; i++)
        targets.push(i);
        
    async.eachSeries(targets, function (target, next) {
        download(target, next);
        start = target;
    }, function (err) {
        if (err == 'Not Found') {
            console.error(err);
            start++;
            setImmediate(main);
        } else if (err) {
            console.warn('RETRY FROM ' + start);
            setImmediate(main);
        } else {
            console.info('DONE');
        }
    });
}



function download(n, callback) {
    var outputDir = setting.outputDir + '/outputs/';
    var saveDir = setting.outputDir + '/' + n;
    var landscape, row, column;
    
    // No. nの画像が存在するか確認
    checkPiece(n, 0, 0, function (err) {
        if (err == null) {
            mkdirp(outputDir, function () {
                mkdirp(saveDir, detectOrientation);
            });
        } else {
            console.log('Not Found: Picture No.' + n);
            callback('Not Found');
        }
    });
    
    function detectOrientation() {
        checkPiece(n, 10, 0, function (err) {
            if (err == null)
                landscape = true;
            else
                landscape = false;
            
            row = (landscape) ? 14 : 10;
            column = (landscape) ? 10 : 14;
                
            downloadAllPieces();
        })
    }
    
    function downloadAllPieces() {
        var arr = (function () {
            var a = [];
            for (var c = 0; c < column; c++) {
                for (var r = 0; r < row; r++) {
                    a.push([r, c]);
                }
            }
            return a;
        })();
        
        async.eachLimit(arr, 70, function (item, next)
        {
            fetchPiece(n, item[0], item[1],
                function (err)
                {
                    if (err == null) next();
                    else if (err == 'Not Found') next('Not Found');
                    else next('Unexpected Error');
                }
            );
        },
        function (err)
        {
            if (err) {
                console.error(err);
                callback(err);
            } else {
                console.info('Downloaded: Picture No.' + n);
                combine();
            }   
        });   
    }
    
    function combine() {
        var filename = n + '.jpg';
        var filepath = outputDir + filename;
        var tileSize = 192;
        
        var g = gm();
        
        for (var c = 0; c < column; c++) {
            for (var r = 0; r < row; r++) {
                var piece = saveDir + '/' + r + '-' + c + '.jpg';
                g.in('-page', '+' + (tileSize * r) + '+' + (tileSize * c))
                .in(piece)
            }
        }

        g.mosaic()  // Merges the images as a matrix
        g.write(filepath, function(err) {
            if(err) {
                console.error(err);
                callback(err);
            } else {
                console.info("Combined: Picture No." + n);
                removePieces();
            }
        });
    }
    
    function removePieces() {
        var paths = [];
        for (var c = 0; c < column; c++) {
            for (var r = 0; r < row; r++) {
                paths.push(saveDir + '/' + r + '-' + c + '.jpg');
            }
        }
        async.each(paths, function (path, next) {
            fs.unlink(path, function (err) {
                if (err) console.error(err);
                else next();
            });
        }, function (err) {
            if (err) {
                console.error(err);
                callback(err);
            } else {
                fs.rmdir(saveDir, function (err) {
                    if (err) {
                        console.error(err);
                        callback(err);
                    } else {
                        console.log('Removed Directory of: Picture No.' + n);
                        callback();
                    }
                });
            }
        });
    }
}

function checkPiece(n, x, y, callback) {
    var targetUrl = TARGET.replace('{{number}}', n).replace('{{x}}', (2 * x + 4 * y)).replace('{{y}}', (4 * x + 2 * y));
    
    var r = request(targetUrl);
    r.on('error', callback);
    r.on('response', function fetch(res) {
        if (res.statusCode >= 200
            && res.statusCode < 300
            && res.headers["content-length"] > 0 )
        {
            callback();
        } else {
            callback('Not Found');
        }
    });
    
}

function fetchPiece(n, x, y, callback) {
    var targetUrl = TARGET.replace('{{number}}', n).replace('{{x}}', (2 * x + 4 * y)).replace('{{y}}', (4 * x + 2 * y));
    var saveFilename = x +'-' + y + '.jpg';
    var saveDir = setting.outputDir + '/' + n;
    var savePath = saveDir + '/' + saveFilename;
    
    var r = request(targetUrl);
    r.on('error', callback);
    r.on('response', fetch);
    
    function fetch(res) {
        if (res.statusCode >= 200
            && res.statusCode < 300
            && res.headers["content-length"] > 0 )
        {
            console.log(res.statusCode, res.statusMessage, saveFilename);
            var stream = fs.createWriteStream(savePath)
                    .on('finish', callback)
                    .on('error', console.error);
            r.pipe(stream);
        } else {
            console.log('Not Found', saveFilename);
            callback('Not Found');
        }
    }
    
}